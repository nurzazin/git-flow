FROM php:7.2-fpm-alpine
WORKDIR /var/www/html
COPY . /
RUN apk update
RUN apk upgrade

RUN apk add zlib-dev
RUN apk add curl
RUN apk add wget
RUN apk add unzip
RUN apk add composer

RUN docker-php-ext-install zip
RUN docker-php-ext-install mysqli
RUN docker-php-ext-install pdo_mysql

RUN apk add --virtual .persistent-deps
RUN apk add --virtual ca-certificates
RUN apk add --virtual curl
RUN apk add --virtual tar
RUN apk add --virtual xz
RUN apk add --virtual libressl
